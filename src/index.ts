const gamepadData = document.getElementById('gamepad-data');
const gamepadConnectionInfo = document.getElementById('gamepad-connection-info');
const gamepadMappingInfo = document.getElementById('gamepad-mapping');

window.addEventListener("gamepadconnected", (e: GamepadEvent) => {
    const info = `Gamepad connected at index ${e.gamepad.index}: ${e.gamepad.id}. ${e.gamepad.buttons.length} buttons, ${e.gamepad.axes.length} axes.`;
    gamepadConnectionInfo.textContent = info;
    console.log(info);
    
    const gamepadInputLoop = () =>{
        gamepadLoop(e.gamepad);
        requestAnimationFrame(gamepadInputLoop);
    }
    requestAnimationFrame(gamepadInputLoop);
});

window.addEventListener("gamepaddisconnected", (e: GamepadEvent) => {
    const info = `Gamepad disconnected from index ${e.gamepad.index}: ${e.gamepad.id}`;
    console.log(info);
    gamepadConnectionInfo.textContent = info;
});

const isButtonPressed = (button: GamepadButton) => typeof (button) == "object" ? button.pressed : button == 1.0;

/**
 * Standard Gamepad Mapping
 * @see https://w3c.github.io/gamepad/#remapping
 */
const standardGamepadMapping : {
    buttons: { [id: number]: string},
    axes: { [id: number]: string},
} = {
    buttons: {
        [0] : "Bottom button in right cluster",
        [1] : "Right button in right cluster",
        [2] : "Left button in right cluster",
        [3] : "Top button in right cluster",
        [4] : "Top left front button",
        [5] : "Top right front button",
        [6] : "Bottom left front button",
        [7] : "Bottom right front button",
        [8] : "Left button in center cluster",
        [9] : "Right button in center cluster",
        [10] : "Left stick pressed button",
        [11] : "Right stick pressed button",
        [12] : "Top button in left cluster",
        [13] : "Bottom button in left cluster",
        [14] : "Left button in left cluster",
        [15] : "Right button in left cluster",
    },
    axes: {
        [0] : "Horizontal axis for left stick (negative left/positive right)",
        [1] : "Vertical axis for left stick (negative up/positive down)",
        [2] : "Horizontal axis for right stick (negative left/positive right)",
        [3] : "Vertical axis for right stick (negative up/positive down)",
    }
};

let axes: number[] = [];
const gamepadLoop = (gamepad: Gamepad) => {
    const g = navigator.getGamepads()[gamepad.index];

    const notify = () => {
        gamepadData.textContent = JSON.stringify({axes: g.axes, buttons: g.buttons.map(isButtonPressed)}, null, 2);
        
        gamepadMappingInfo.textContent = JSON.stringify({
            axes: g.axes.reduce((acc, axis, i) => ({ ...acc, [standardGamepadMapping.axes[i]]: axis }), {}), 
            buttons: g.buttons.map(isButtonPressed)
                .map((pressed, i) => pressed?standardGamepadMapping.buttons[i] : null )
                .filter(label => label!==null)
        }, null, 2);
    }

    g.buttons.forEach( (button, i) => {
        if(isButtonPressed(button)){
            notify()
        }
    });

    if(axes.length != g.axes.length)
        axes = [...g.axes];

    const axisChangeTreshold = 0.1
    
    if(axes.map( (e, i) => Math.abs(e - g.axes[i]) ).filter(e => e > axisChangeTreshold).length > 0 ){
        axes = [...g.axes];
        notify()
    }

    //(<any>window).gamepad = gamepad;
}
