# Html5 GamepadApi Demo
A small example how to use the [HTML5 Gamepad Api] (https://w3c.github.io/gamepad/).
(E.g. with Xbox One Controller)

See Demo on [https://tim-s.gitlab.io/html5gamepadapidemo/](https://tim-s.gitlab.io/html5gamepadapidemo/)

## Prerequisites

All you need to get started is [Node.js](https://nodejs.org/) and [npm](https://www.npmjs.com/get-npm).

## Install

Just download the source or clone this repository 

Then install dependencies:
```
npm install
```

## Run

```
npm start
```

## License

This package is released under [Unlicense](http://unlicense.org) meaning you can do pretty much anything you want with it.
